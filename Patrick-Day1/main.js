//Day 1

// challenge 1 - Print 'Hello World' to the console
console.log("Hello World!");

// Challenge 2 - Meet Yourself - Declare Three varaibles
const firstName = "Patrick";
const age = 28;
const isExcited = true;

// Print the values of the variables above using descriptive messages
console.log(`It is ${isExcited} that I am ${firstName} with age of ${age} years.`);

//challenge 3 - Age Check. 
/* Write an if statement that checks if your age is greater than 18. If true, print "You're eligible to learn advanced concepts!" to the console. */
if(age > 18){
    console.log("You're eligible to learn advanced concepts!");
}
