// Day 2: Variables and Data Types

// Challenge 1: Multi-talented Variables
// Declare variables of the following data types and assign appropriate values:
const favFood = "Chicken Pilau";
const currentYear = 2024;
const dayCoding = true;
// print the variable data type and the value assignment
console.log(`${favFood} is a ` + typeof(favFood));

console.log(`${currentYear} is a ` + typeof(currentYear));

console.log(`${dayCoding} is a ` + typeof (dayCoding));

// Challenge 2: Mathematical Operations
/* Declare two numeric variables and perform basic mathematical operations on them (addition, subtraction, multiplication, division). Print the results to the console. */
const num1 = 20;
const num2 = 10;
// Addition
const addNums = num1 + num2;
console.log(addNums);
// Subtraction
const subNums = num1 - num2;
console.log(subNums);
// Multiplication
const mulNums = num1 * num2;
console.log(mulNums);
// Division
const divNums = num1 / num2;
console.log(divNums);

// Challenge 3:  Mixing It Up
// Can you concatenate (join) a string and a number? Try creating a message like "The current year is: 2024".
const stringValue = "The current year is: ";
const numValue = 2024;
const message = stringValue + numValue;
console.log(message);
